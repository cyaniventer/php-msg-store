# php-msg-store

php-msg-store is a simple php program that stores user's message
PostgreSQL database.

I am using `php-signup-login` for this.

I have tried to fix SQL Injection, XSS, spam bots but you should check
it yourself before using it for critical purpose. I'll look into CSRF
later when I get time.

Contributions are encouraged, that might help me learn something new.

PostgreSQL database should be pre-setup and users should edit the values
in signup-login.php before using it.

## columns in table - users

| columns       | type          | is NULL  | other                |
| ------------- |---------------| ---------| ---------------------|
| username      | varchar       | NO       | unique               |
| password      | varchar       | NO       | limit: 255 (optional)|
| email         | varchar       |          |                      |
| message       | varchar       |          |                      |

you should decide character limit for other columns yourself

`pgsql` and `pdo_pgsql` plugin should be enabled in `php.ini`, you can
enable it by editing `/etc/php/php.ini` and uncommenting the following
line: 

```
extension=pgsql
extension=pdo_pgsql
```

more information on this: wiki.archlinux.org/index.php/PHP#PostgreSQL
